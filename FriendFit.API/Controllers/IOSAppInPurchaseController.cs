﻿using FriendFit.API.Filters;
using FriendFit.API.Models;
using FriendFit.API.PayPal;
using FriendFit.Data;
using FriendFit.Data.ApiModel.APIRequestModel;
using FriendFit.Data.ApiModel.APIResponseModel;
using FriendFit.Data.IRepository;
using FriendFit.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;

namespace FriendFit.API.Controllers
{
    [RoutePrefix("api/iOSAppInPurchase")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class IOSAppInPurchaseController : ApiController
    {
        private FriendFit.Data.FriendFitDBContext _objFriendFitDBEntity = new FriendFit.Data.FriendFitDBContext();
        public invoiceRepository Inovoice = new invoiceRepository();
        private _EmailController _ObjEmail = new _EmailController();
        private TwilioSMSDemoController SMSCont = new TwilioSMSDemoController();

        [HttpPost]
        //Get payment details
        [Route("GetPaymentDetails_User")]
        [SecureResource]
        public AddActualResponse GetPaymentDetails_User(IOSAfterPaymentRequestModel ObjiOSAfterPaymentRequestModel)
        {
            AddActualResponse ObjAddActualResponse = new AddActualResponse();
            var headers = Request.Headers;
            string token = headers.Authorization.Parameter.ToString();
            Int64 UserId = _objFriendFitDBEntity.Database.SqlQuery<Int64>("select UserId from UserToken where TokenCode={0}", token).FirstOrDefault();

            var GetUserInfo = _objFriendFitDBEntity.UserProfiles.Where(s => s.Id == UserId).FirstOrDefault();
            string str_payment = "";
            string FileSavedResult = "";
            string Result = "";
            try
            {
                double? price = 0;
                if (ObjiOSAfterPaymentRequestModel.SubscriptionType == "SMS")
                {
                    if (ObjiOSAfterPaymentRequestModel.SubscriptionReminder == "recurring")
                    {
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == ObjiOSAfterPaymentRequestModel.SubscriptionDuration && x.IsSMS == "Yes").FirstOrDefault().Recurring;
                    }
                    else
                    {
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == ObjiOSAfterPaymentRequestModel.SubscriptionDuration && x.IsSMS == "Yes").FirstOrDefault().One_Off;
                    }
                }
                else if (ObjiOSAfterPaymentRequestModel.SubscriptionType == "App")
                {
                    if (ObjiOSAfterPaymentRequestModel.SubscriptionReminder == "recurring")
                    {
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == ObjiOSAfterPaymentRequestModel.SubscriptionDuration && x.IsSMS == "No").FirstOrDefault().Recurring;
                    }
                    else
                    {
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == ObjiOSAfterPaymentRequestModel.SubscriptionDuration && x.IsSMS == "No").FirstOrDefault().One_Off;
                    }
                }
                string SKU_UserInvitations = GetPaymentPayPalURL_user(ObjiOSAfterPaymentRequestModel, GetUserInfo.Id, price);
                UserProductPayment tbl_payment = new UserProductPayment();

                var payPalPaymentIdExist = _objFriendFitDBEntity.UserProductPayments.Where(s => s.PaypalId == ObjiOSAfterPaymentRequestModel.PurchaseId).FirstOrDefault();

                if (payPalPaymentIdExist == null)
                {
                    //  var GetPay
                    tbl_payment.PaypalId = ObjiOSAfterPaymentRequestModel.PurchaseId;
                    tbl_payment.Intent = "sale-iOS";
                    tbl_payment.Cart = "iOS";
                    tbl_payment.State = ObjiOSAfterPaymentRequestModel.PurchaseState;
                    tbl_payment.Create_time = Convert.ToString(DateTime.UtcNow);
                    tbl_payment.Update_time = Convert.ToString(DateTime.UtcNow);
                    tbl_payment.Payer_PaymentMethod = "App-in-Purchase";
                    tbl_payment.Status = "payment Done iOS";
                    tbl_payment.Payer_info_Email = GetUserInfo.Email;
                    tbl_payment.Payer_info_FirstName = GetUserInfo.FirstName;
                    tbl_payment.Payer_info_LastName = GetUserInfo.LastName;
                    tbl_payment.Payer_info_Payer_Id = "";
                    tbl_payment.Payer_info_Country_Code = Convert.ToString(GetUserInfo.CountryId);

                    tbl_payment.Shipping_Address_Recipient_Name = GetUserInfo.FirstName + " " + GetUserInfo.LastName;
                    tbl_payment.Shipping_Address_Line1 = "";
                    tbl_payment.Shipping_Address_Line2 = "";
                    tbl_payment.Shipping_Address_City = "";
                    tbl_payment.Shipping_Address_Country_Code = Convert.ToString(GetUserInfo.CountryId);
                    tbl_payment.Shipping_Address_PostedCode = "";
                    tbl_payment.Shipping_Address_State = "";

                    tbl_payment.Transactions_Description = "";
                    tbl_payment.Transactions_InvoiceNum = ObjiOSAfterPaymentRequestModel.ProductId;
                    tbl_payment.RowInsert = DateTime.UtcNow;
                    tbl_payment.UserId = Convert.ToInt32(UserId);

                    _objFriendFitDBEntity.UserProductPayments.Add(tbl_payment);
                    _objFriendFitDBEntity.SaveChanges();

                    int payid = _objFriendFitDBEntity.UserProductPayments.Where(x => x.PaypalId == ObjiOSAfterPaymentRequestModel.PurchaseId).FirstOrDefault().Id;


                    UserPurchaseProductsList pr = new UserPurchaseProductsList();
                    pr.PaymentId = payid;
                    pr.SKU = SKU_UserInvitations;
                    pr.Name = ObjiOSAfterPaymentRequestModel.ProductId + "--" + ObjiOSAfterPaymentRequestModel.SubscriptionReminder + "---" + price + "---" + ObjiOSAfterPaymentRequestModel.SubscriptionType;
                    pr.Quantity = "1";
                    pr.Price = Convert.ToString(price);
                    pr.Currency = "USD";
                    pr.RowInsert = DateTime.UtcNow;
                    _objFriendFitDBEntity.UserPurchaseProductsLists.Add(pr);
                    _objFriendFitDBEntity.SaveChanges();

                    Int64 newsku = Convert.ToInt64(SKU_UserInvitations);
                    var IsPaymentDone = _objFriendFitDBEntity.UserInvitations.Where(x => x.Id == newsku).FirstOrDefault();
                    if (IsPaymentDone != null)
                    {
                        IsPaymentDone.IsActive = true;
                        IsPaymentDone.IsRowActive = true;
                        IsPaymentDone.PaymentDone = 1;
                        _objFriendFitDBEntity.Entry(IsPaymentDone).State = EntityState.Modified;
                        _objFriendFitDBEntity.SaveChanges();

                        // var GetUserdetail = _objFriendFitDBEntity.UserProfiles.Where(x => x.Id == IsPaymentDone.Userid).FirstOrDefault();
                        EmailModelAttachment tm = new EmailModelAttachment();
                        tm.ToEmail = GetUserInfo.Email;
                        if (GetUserInfo.LastName != null)
                        {
                            tm.CustomerName = GetUserInfo.FirstName + ' ' + GetUserInfo.LastName;
                        }
                        else
                        {
                            tm.CustomerName = GetUserInfo.FirstName;
                        }
                        tm.PayPalPaymentId = ObjiOSAfterPaymentRequestModel.PurchaseId;
                        tm.Subject = "Notifit Inovice";
                        tm.FileURL = "/Invoice/" + ObjiOSAfterPaymentRequestModel.PurchaseId + ".pdf";
                        tm.IsSMS = ObjiOSAfterPaymentRequestModel.SubscriptionType;
                        tm.Isrecurringmonthly = ObjiOSAfterPaymentRequestModel.SubscriptionReminder;
                        tm.ProductAmount = Convert.ToString(price);

                        if (GetUserInfo.CountryId == 61) // Australia
                        {
                            tm.includingGST = "Total price in USD including GST";
                        }
                        else
                        {
                            tm.includingGST = "Total price in USD";
                        }
                        tm.TotalAmount = Convert.ToString(price);
                        FileSavedResult = Inovoice.DownloadApplicationPDF(tm);
                        Result = Inovoice.SendEmailWithAttachment(tm);
                    }
                    ObjAddActualResponse.Response.Message = "Payment success";
                    ObjAddActualResponse.Response.StatusCode = 200;
                }
                else
                {
                    ObjAddActualResponse.Response.Message = "Payment Id Alrady Exists!";
                    ObjAddActualResponse.Response.StatusCode = 201;
                }
            }
            catch (Exception ex)
            {
                ObjAddActualResponse.Response.Message = "Sorry there is an error getting the payment details. " + ex;
                ObjAddActualResponse.Response.StatusCode = 401;
            }
            return ObjAddActualResponse;//str_payment + "------" + FileSavedResult + "-------------" + Result;
        }

        [HttpPost]
        //Get payment details
        [Route("GetPaymentDetails")]
        [SecureResource]
        public AddActualResponse GetPaymentDetails(IOSAfterPaymentFriendRequestModel ObjiOSAfterPaymentFriendRequestModel)
        {
            AddActualResponse ObjAddActualResponse = new AddActualResponse();
            var headers = Request.Headers;
            string token = headers.Authorization.Parameter.ToString();
            Int64 UserId = _objFriendFitDBEntity.Database.SqlQuery<Int64>("select UserId from UserToken where TokenCode={0}", token).FirstOrDefault();

            string str_payment = "";
            string FileSavedResult = "";
            string Result = "";
            try
            {
                int GetCountOfFriendsPayment = _objFriendFitDBEntity.FriendsInvitations.Where(s => s.UserId == UserId && s.PaymentDone == 1 && s.ExpiryDate >= DateTime.Now).Count();
                if (GetCountOfFriendsPayment<=5)
                {
                    var GetUserInfo = _objFriendFitDBEntity.UserProfiles.Where(s => s.Id == UserId).FirstOrDefault();
                    //-----Data Friend Data
                    ResponseFriendDataForIOS ObjResponseFriendDataForIOS = SaveFriendData(ObjiOSAfterPaymentFriendRequestModel.ObjFriendDataForIOS, GetUserInfo.FirstName, GetUserInfo.Id);
                    //-----Data Friend Data

                    Payment tbl_payment = new Payment();
                    var payPalPaymentIdExist = _objFriendFitDBEntity.Payments.Where(s => s.PaypalId == ObjiOSAfterPaymentFriendRequestModel.PurchaseId).FirstOrDefault();

                    if (payPalPaymentIdExist == null)
                    {
                        //  var GetPay
                        tbl_payment.PaypalId = ObjiOSAfterPaymentFriendRequestModel.PurchaseId;
                        tbl_payment.Intent = "sale-iOS";
                        tbl_payment.Cart = "iOS";
                        tbl_payment.State = ObjiOSAfterPaymentFriendRequestModel.PurchaseState;
                        tbl_payment.Create_time = Convert.ToString(DateTime.UtcNow);
                        tbl_payment.Update_time = Convert.ToString(DateTime.UtcNow);
                        tbl_payment.Payer_PaymentMethod = "App-in-Purchase";
                        tbl_payment.Status = "payment Done iOS";
                        tbl_payment.Payer_info_Email = GetUserInfo.Email;
                        tbl_payment.Payer_info_FirstName = GetUserInfo.FirstName;
                        tbl_payment.Payer_info_LastName = GetUserInfo.LastName;
                        tbl_payment.Payer_info_Payer_Id = "";
                        tbl_payment.Payer_info_Country_Code = Convert.ToString(GetUserInfo.CountryId);

                        tbl_payment.Shipping_Address_Recipient_Name = GetUserInfo.FirstName + " " + GetUserInfo.LastName;
                        tbl_payment.Shipping_Address_Line1 = "";
                        tbl_payment.Shipping_Address_Line2 = "";
                        tbl_payment.Shipping_Address_City = "";
                        tbl_payment.Shipping_Address_Country_Code = Convert.ToString(GetUserInfo.CountryId);
                        tbl_payment.Shipping_Address_PostedCode = "";
                        tbl_payment.Shipping_Address_State = "";

                        tbl_payment.Transactions_Description = "";
                        tbl_payment.Transactions_InvoiceNum = ObjiOSAfterPaymentFriendRequestModel.ProductId;
                        tbl_payment.RowInsert = DateTime.UtcNow;
                        tbl_payment.UserId = Convert.ToInt32(UserId);

                        _objFriendFitDBEntity.Payments.Add(tbl_payment);
                        _objFriendFitDBEntity.SaveChanges();


                        int payid = _objFriendFitDBEntity.Payments.Where(x => x.PaypalId == ObjiOSAfterPaymentFriendRequestModel.PurchaseId).FirstOrDefault().Id;

                        PurchaseProductsList pr = new PurchaseProductsList();
                        pr.PaymentId = payid;
                        pr.SKU = ObjiOSAfterPaymentFriendRequestModel.PurchaseId;
                        pr.Name = ObjiOSAfterPaymentFriendRequestModel.ProductId;
                        pr.Quantity = "1";
                        pr.Price = Convert.ToString(ObjResponseFriendDataForIOS.price);
                        pr.Currency = "USD";
                        pr.RowInsert = DateTime.UtcNow;

                        _objFriendFitDBEntity.PurchaseProductsLists.Add(pr);
                        _objFriendFitDBEntity.SaveChanges();

                        var IsPaymentDone = _objFriendFitDBEntity.FriendsInvitations.Where(x => x.Id == ObjResponseFriendDataForIOS.SKU).FirstOrDefault();
                        if (IsPaymentDone != null)
                        {
                            IsPaymentDone.IsActive = true;
                            IsPaymentDone.IsRowActive = true;
                            IsPaymentDone.PaymentDone = 1;
                            _objFriendFitDBEntity.Entry(IsPaymentDone).State = EntityState.Modified;
                            _objFriendFitDBEntity.SaveChanges();
                        };

                        //-------------------------for Inovice
                        int PaymentRowId = _objFriendFitDBEntity.Payments.Where(x => x.PaypalId == ObjiOSAfterPaymentFriendRequestModel.PurchaseId).FirstOrDefault().Id;
                        var GetAllRows_Payment = _objFriendFitDBEntity.PurchaseProductsLists.Where(x => x.PaymentId == PaymentRowId).ToList();

                        string FriendsHTML = "";
                        EmailModelAttachment tm = new EmailModelAttachment();
                        var GetUserdetail = _objFriendFitDBEntity.UserProfiles.Where(a => a.Id == tbl_payment.UserId).FirstOrDefault();
                        decimal TotalAmt = 0;
                        foreach (var Payment_item in GetAllRows_Payment)
                        {
                            long id = Convert.ToInt64(Payment_item.SKU);
                            var GetuserId = _objFriendFitDBEntity.FriendsInvitations.Where(x => x.Id == id).FirstOrDefault();

                            tm.IsSMS = ObjiOSAfterPaymentFriendRequestModel.ObjFriendDataForIOS.SubscriptionReminder;
                            tm.Isrecurringmonthly = ObjiOSAfterPaymentFriendRequestModel.ObjFriendDataForIOS.SubscriptionReminder;
                            tm.ProductAmount = Convert.ToString(ObjResponseFriendDataForIOS.price);

                            // tm.IsSMS = _objFriendFitDBEntity.DeliveryTypeMasters.Where(x => x.Id == GetuserId.DeliveryTypeId).FirstOrDefault().Name;
                            // tm.Isrecurringmonthly = _objFriendFitDBEntity.SubscriptionTypeMasters.Where(x => x.Id == GetuserId.SubscriptionTypeId).FirstOrDefault().SubcriptionType;
                            // tm.ProductAmount = Convert.ToString(GetuserId.Cost.Value.ToString("0.00"));
                            //TotalAmt += Convert.ToDecimal(GetuserId.Cost.Value.ToString("0.00"));
                            TotalAmt += Convert.ToDecimal(ObjResponseFriendDataForIOS.price.Value.ToString("0.00"));
                        }
                        tm.ToEmail = GetUserdetail.Email;
                        tm.FriendsHTML = FriendsHTML;
                        if (GetUserdetail.LastName != null)
                            tm.CustomerName = GetUserdetail.FirstName + ' ' + GetUserdetail.LastName;
                        else
                            tm.CustomerName = GetUserdetail.FirstName;

                        tm.PayPalPaymentId = ObjiOSAfterPaymentFriendRequestModel.PurchaseId;
                        tm.Subject = "Notifit Friend Inovice";
                        tm.FileURL = "/FriendInvoice/" + ObjiOSAfterPaymentFriendRequestModel.PurchaseId + ".pdf";

                        if (GetUserdetail.CountryId == 61)
                        {
                            tm.includingGST = "Total price in USD including GST";
                        }
                        else
                        {
                            tm.includingGST = "Total price in USD ";
                        }

                        tm.TotalAmount = Convert.ToString(TotalAmt);
                        FileSavedResult = Inovoice.DownloadApplicationPDF_Friend(tm);
                        Result = Inovoice.SendEmailWithAttachment(tm);
                        ObjAddActualResponse.Response.Message = "Payment Success";
                        ObjAddActualResponse.Response.StatusCode = 200;
                    }
                    else
                    {
                        ObjAddActualResponse.Response.Message = "Payment Id already exists";
                        ObjAddActualResponse.Response.StatusCode = 201;
                    }
                }
                else
                {
                    ObjAddActualResponse.Response.Message = "Sorry! You have exceeded the maximum 5 transaction limit.";
                    ObjAddActualResponse.Response.StatusCode = 201;
                }
               
            }
            catch (Exception ex)
            {
                ObjAddActualResponse.Response.Message = "Sorry there is an error getting the payment details.";
                ObjAddActualResponse.Response.StatusCode = 201;
            }
            return ObjAddActualResponse;// str_payment + "------" + FileSavedResult + "-------------" + Result;
        }

        public string GetPaymentPayPalURL_user(IOSAfterPaymentRequestModel ObjiOSAfterPaymentRequestModel, long UserId, double? price)
        {
            try
            {
                UserInvitation _ui = new UserInvitation();
                if (ObjiOSAfterPaymentRequestModel.SubscriptionType == "App")
                {
                    _ui.DeliveryTypeId = 1;
                }
                else
                {
                    _ui.DeliveryTypeId = 2;
                }
                //////////////
                if (ObjiOSAfterPaymentRequestModel.SubscriptionReminder == "recurring")
                {
                    _ui.SubscriptionTypeId = 1;
                }
                else if (ObjiOSAfterPaymentRequestModel.SubscriptionReminder == "One Off")
                {
                    _ui.SubscriptionTypeId = 2;
                }
                else
                {
                    _ui.SubscriptionTypeId = 3;
                }
                //////////////
                _ui.Userid = UserId;
                _ui.DurationId = 1;
                _ui.PurchaseDate = DateTime.Now;
                _ui.ExpiryDate = DateTime.Now.AddMonths(1);//By Default Duration is One Month
                _ui.Cost = Convert.ToDecimal(price);
                _ui.IsActive = false;
                _ui.IsRowActive = false;
                _ui.PaymentDone = 0;
                _objFriendFitDBEntity.UserInvitations.Add(_ui);
                _objFriendFitDBEntity.SaveChanges();
                string sku = Convert.ToString(_ui.Id);
                return sku;
            }
            catch (Exception ex)
            {
                return Convert.ToString(ex);
            }
        }

        public ResponseFriendDataForIOS SaveFriendData(FriendDataForIOS objFriendInvitation, string UserName, long UserId)
        {
            ResponseFriendDataForIOS ObjResponseFriendDataForIOS = new ResponseFriendDataForIOS();
            try
            {
                FriendInvitationRepository ObjFriendInvitationRepository = new FriendInvitationRepository();
                DateTime PurchaseDate = DateTime.Now;
                DateTime ExpiryDate = DateTime.Now.AddMonths(1);
                double? price = 0;
                int DeliveryTypeId = 0;
                int SubscriptionTypeId = 0;
                if (objFriendInvitation.SubscriptionType == "SMS")
                {
                    DeliveryTypeId = 2;
                    if (objFriendInvitation.SubscriptionReminder == "recurring")
                    {
                        SubscriptionTypeId = 1;
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == "1 month" && x.IsSMS == "Yes").FirstOrDefault().Recurring;
                    }
                    else
                    {
                        SubscriptionTypeId = 2;
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == "1 month" && x.IsSMS == "Yes").FirstOrDefault().One_Off;
                    }
                }
                else if (objFriendInvitation.SubscriptionType == "App")
                {
                    DeliveryTypeId = 1;
                    if (objFriendInvitation.SubscriptionReminder == "recurring")
                    {
                        SubscriptionTypeId = 1;
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == "1 month" && x.IsSMS == "No").FirstOrDefault().Recurring;
                    }
                    else
                    {
                        SubscriptionTypeId = 2;
                        price = _objFriendFitDBEntity.tblPrices.Where(x => x.Duration == "1 month" && x.IsSMS == "No").FirstOrDefault().One_Off;
                    }
                }
                decimal Cost = Convert.ToDecimal(price);
                int rowEffected = _objFriendFitDBEntity.Database.ExecuteSqlCommand("AddFriendInvitation @UserId=@UserId,@DeliveryTypeId=@DeliveryTypeId,@FriendsName=@FriendsName,@DurationId=@DurationId,@PurchaseDate=@PurchaseDate,@ExpiryDate=@ExpiryDate,@SubscriptionTypeId=@SubscriptionTypeId,@Cost=@Cost,@Email=@Email,@MobileNumber=@MobileNumber,@CountryId=@CountryId",
                                                                    new SqlParameter("UserId", UserId),
                                                                    new SqlParameter("DeliveryTypeId", DeliveryTypeId),
                                                                    new SqlParameter("FriendsName", objFriendInvitation.FriendsName),
                                                                    new SqlParameter("DurationId", 1),
                                                                    new SqlParameter("PurchaseDate", PurchaseDate),
                                                                    new SqlParameter("ExpiryDate", ExpiryDate),
                                                                    new SqlParameter("SubscriptionTypeId", SubscriptionTypeId),
                                                                    new SqlParameter("Cost", Cost),
                                                                    new SqlParameter("Email", (Object)objFriendInvitation.Email ?? DBNull.Value),
                                                                    new SqlParameter("MobileNumber", (Object)objFriendInvitation.MobileNumber ?? DBNull.Value),
                                                                    new SqlParameter("CountryId", (Object)objFriendInvitation.CountryId ?? DBNull.Value));


                string RegistrationUrl = WebConfigurationManager.AppSettings["FrendFitSignUp"];
                #region send mail for notification
                if (objFriendInvitation.Email != null)
                {
                    if (objFriendInvitation.Email != "")
                    {
                        var SendingMessage = new MailMessage();
                        SendingMessage.To.Add(new MailAddress(objFriendInvitation.Email));  // replace with valid value
                        SendingMessage.From = new MailAddress("noreply@noti.fit");  // replace with valid value
                        SendingMessage.Subject = "Notification your email (noti.fit)";
                        SendingMessage.Body = "<p>Hi " + objFriendInvitation.FriendsName + "</p><p>Your friend " + objFriendInvitation.Email + " has invited you to track their workouts on noti.fit!noti.fit is a workout tracker that sends you notifications when your friend misses a workout.</ p >< p > Please sign up at " + RegistrationUrl + " to get started!</ p >< p > Cheers<strong>,</ strong >< br /> The noti.fit tea</ p > ";

                        SendingMessage.IsBodyHtml = true;
                        using (var smtp = new SmtpClient())
                        {
                            var credential = new NetworkCredential
                            {
                                UserName = "testifiedemail@gmail.com",  // replace with valid value
                                Password = "testifiedpassword@hackfree"  // replace with valid value
                            };
                            smtp.Credentials = credential;
                            smtp.Host = "smtp.gmail.com";
                            smtp.Port = 587;
                            smtp.EnableSsl = true;
                            smtp.Send(SendingMessage);
                        }
                    }
                }
                #endregion

                #region notification via sms
                if (objFriendInvitation.MobileNumber != null)
                {
                    if (objFriendInvitation.MobileNumber != "")
                    {
                        twilioModel tm = new twilioModel();
                        tm.mobileNo = objFriendInvitation.MobileNumber;
                        tm.messagebody = "Hi " + UserName + ", " + objFriendInvitation.FriendsName + " has nominated you to receive their workout reminders. Please accept/ decline at " + RegistrationUrl + " or ignore this text for no further communication.";
                        var SMSStatus = SMSCont.SendSMS(tm);
                    }
                }
                #endregion
                var FriendSKU = _objFriendFitDBEntity.FriendsInvitations.Where(s => s.UserId == UserId).OrderByDescending(x => x.Id).FirstOrDefault();
                ObjResponseFriendDataForIOS.DeliveryTypeId = DeliveryTypeId;
                ObjResponseFriendDataForIOS.SubscriptionTypeId = SubscriptionTypeId;
                ObjResponseFriendDataForIOS.price = price;
                ObjResponseFriendDataForIOS.SKU = FriendSKU.Id;
            }
            catch (Exception ex)
            {
            }
            return ObjResponseFriendDataForIOS;
        }

        [HttpPost]
        //Get payment details
        [Route("GetCountOfFriendsPayment")]
        [SecureResource]
        public int GetCountOfFriendsPayment()
        {
            var headers = Request.Headers;
            string token = headers.Authorization.Parameter.ToString();
            Int64 UserId = _objFriendFitDBEntity.Database.SqlQuery<Int64>("select UserId from UserToken where TokenCode={0}", token).FirstOrDefault();
            int GetCountOfFriendsPayment = _objFriendFitDBEntity.FriendsInvitations.Where(s => s.UserId == UserId && s.PaymentDone == 1 && s.ExpiryDate >= DateTime.Now).Count();
            return GetCountOfFriendsPayment;
        }
    }
}
