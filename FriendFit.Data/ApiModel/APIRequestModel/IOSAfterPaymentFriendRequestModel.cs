﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriendFit.Data.ApiModel.APIRequestModel
{
   public class IOSAfterPaymentFriendRequestModel
    {
        //public int NumOfPayment { get; set; } //
        //public DateTime TransactionDateUtc { get; set; } //
        public string ProductId { get; set; }//
        public bool AutoRenewing { get; set; }
        public string PurchaseToken { get; set; }//
        public string PurchaseState { get; set; }//
        public string Payload { get; set; }//
        public string PurchaseId { get; set; }//
        public string SubscriptionDuration { get; set; }
        public FriendDataForIOS ObjFriendDataForIOS { get; set; }
    }

    public class FriendDataForIOS
    {
        public string FriendsName { get; set; }       
        public Int64? CountryId { get; set; }        
        public string Email { get; set; }
        public string MobileNumber { get; set; }

        public string SubscriptionType { get; set; } // App/SMS
        public string SubscriptionReminder { get; set; } //recurring/OneOff
    }

    public class ResponseFriendDataForIOS
    {
        public double? price { get; set; }
        public int DeliveryTypeId { get; set; }
        public int SubscriptionTypeId { get; set; }
        public long SKU { get; set; }
    }
}