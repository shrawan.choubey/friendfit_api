﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriendFit.Data.ApiModel.APIRequestModel
{
    public class IOSAfterPaymentRequestModel
    {
        /// <summary>
        /// Trasaction date in UTC
        /// </summary>
        public DateTime TransactionDateUtc { get; set; }

        /// <summary>
        /// Product Id/Sku
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// Indicates whether the subscritpion renewes automatically. If true, the sub is active, else false the user has canceled.
        /// </summary>
        public bool AutoRenewing { get; set; }

        /// <summary>
        /// Unique token identifying the purchase for a given item
        /// </summary>
        public string PurchaseToken { get; set; }

        /// <summary>
        /// Gets the current purchase/subscription state
        /// </summary>
        public string PurchaseState { get; set; }

        /// <summary>
        /// Developer payload
        /// </summary>
        public string Payload { get; set; }

        public string PurchaseId { get; set; }
        public string SubscriptionType { get; set; }// App/SMS
        public string SubscriptionReminder { get; set; } //recurring/OneOff
        public string SubscriptionDuration { get; set; }
    }
}
